package com.example.kalkulat;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    TextView result;
    EditText number;
    TextView operation;
    Double operand = null;
    String last = "=",  prc;
    TableRow need;
    TextView secret;
    Button SIN;
    Button COS;
    Button TAN;
    Button STEPEN;
    Button KOREN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        result = (TextView) findViewById(R.id.textView);
        number = (EditText) findViewById(R.id.editText);
        operation = (TextView) findViewById(R.id.textView2);
        getScreenOrientation();
        need = (TableRow) findViewById(R.id.idid);
        SIN = findViewById(R.id.button22);
        COS = findViewById(R.id.button23);
        TAN = findViewById(R.id.button24);
        STEPEN = findViewById(R.id.button25);
        KOREN = findViewById(R.id.button26);
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("OPERATION", last);
        if(operand!=null)
            outState.putDouble("OPERAND", operand);
        super.onSaveInstanceState(outState);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        last = savedInstanceState.getString("OPERATION");
        operand= savedInstanceState.getDouble("OPERAND");
        result.setText(operand.toString());
        operation.setText(last);
    }
    private void getScreenOrientation() {
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
        {

        }
        else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
        {

        }
    }


    public void anotherButtonGotClicked(View view)
    {
        number.getText().clear();
        result.setText("");
        operation.setText("");
    }

    public void anotherOperationGotClicked(View view) {
////        secret.setText();
////        switch(secret)
////        {
//            //case "%":operand = Double.valueOf(number.getText().toString()) /100;break;
//            /*case "SIN":operand=Math.sin(number.getText());break;
//            case "COS":operand=Math.cos(number.getText());break;
//            case "TG":operand=Math.tan(number.getText());break;
//            case "LN":operand=Math.log(number.getText());break;*/
//        }
        }
    private void perform(Double num, String operation){
        if(operand == null)
        {
            operand = num;
        }
        else{
            if(last.equals("=")){
                last = operation;
            }
            switch(last)
            {
                case "=":operand = num; break;
                case "/":if(num==0){
                    operand = 0.0;
                }
                else{
                    operand /=num;
                }break;
                case "*":operand *=num;break;
                case "+":operand +=num;break;
                case "-":operand -=num;break;
                case "%":operand =num/100;break;
            }
        }
        result.setText(operand.toString().replace(".", ","));
        number.setText("");
    }

    public void operationGotClicked(View view)
    {
        Button button = (Button)view;
        String op = button.getText().toString();
        String num = number.getText().toString();
        if(num.length()>0)
        {
            num = num.replace(",", ".");
            try {
                perform(Double.valueOf(num), op);
            }
            catch (NumberFormatException ex)
            {
                number.setText("");
            }
        }
        last = op;
        operation.setText(last);

    }

    public void backGotClicked(View view) {
        if(number.length()>0) {
            StringBuffer sb = new StringBuffer(number.getText().toString());
            sb.delete(sb.length() - 1, sb.length());
            number.setText(sb.toString());
        }
        else{

        }
    }

    public void numberGotClicked(View view)
    {
        Button button = (Button)view;
        number.append(button.getText());
        if(last.equals("=") && operand!=null)
        {
            operand = null;
        }
    }

    public void reverseGotClicked(View view) {
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            getResources().getConfiguration().orientation = Configuration.ORIENTATION_LANDSCAPE;
        }
        else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            getResources().getConfiguration().orientation = Configuration.ORIENTATION_PORTRAIT;
        }
    }

    public void plusMINUSplus(View view) {

        if(Integer.valueOf(number.getText().toString())==0 || Integer.valueOf(number.getText().toString())==null)
        {

        }
        else if(number.getText().toString().contains("-")) {
            number.setText(number.toString().replace("-", ""));
        }
        else if(number.getText().toString().contains("-"))
        {
            number.setText("-" + number.getText().toString());
        }
    }

    public void procent(View view) {

    }
}
